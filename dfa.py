from typing import List, Tuple, Set, Dict
from itertools import count, repeat
from graphviz import Digraph, render

class RATNode: # Regular expression Analysis Tree
    value: str
    left: 'RATNode'
    right: 'RATNode'
    nullable: bool
    firstpos: List[int]
    lastpos: List[int]

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def is_leaf(self):
        return (not self.left) and (not self.right)

    def dft(self, func):
        'Depth-first traversal'
        if self.left:
            self.left.dft(func)
        if self.right:
            self.right.dft(func)
        func(self)

def indexing_leaves():
    counter = count(1)
    pos_table = dict()

    def dft_feed(node):
        if node.is_leaf():
            i = next(counter)
            node.firstpos = node.lastpos = {i}
            node.nullable = False
            pos_table[i] = node.value

    return (pos_table, dft_feed)

def eval_nullable_firstpos_lastpos(node):
    if node.value == '|':
        node.nullable = node.left.nullable or node.right.nullable
        node.firstpos = node.left.firstpos.union(node.right.firstpos)
        node.lastpos = node.left.lastpos.union(node.right.lastpos)
    elif node.value == '*':
        node.nullable = True
        node.firstpos = node.left.firstpos
        node.lastpos = node.left.lastpos
    elif node.value == '.':
        node.nullable = node.left.nullable and node.right.nullable
        node.firstpos = (node.left.firstpos.union(node.right.firstpos) 
            if node.left.nullable else node.left.firstpos
        )
        node.lastpos = (node.right.lastpos.union(node.left.lastpos) 
            if node.right.nullable else node.right.lastpos
        )

def eval_followpos(maxpos):
    result = dict()
    for i in range(1, maxpos):
        result[i] = set()

    def dft_feed(node):
        if node.value == '*':
            for lpos in node.lastpos:
                for fpos in node.firstpos:
                    result[lpos].add(fpos)
        elif node.value == '.':
            for lpos in node.left.lastpos:
                for fpos in node.right.firstpos:
                    result[lpos].add(fpos)

    return (result, dft_feed)

def next_state(state, char, followpos_table, pos_table):
    result = set()
    for pos in state:
        if pos_table[pos] == char:
            result = result.union(followpos_table[pos])

    return result

def build_DFA(initial_state, followpos_table, pos_table, maxpos):

    all_chars = set(pos_table.values())
    all_chars.remove('#')

    DFA_list = list()
    DFA_list.append(initial_state)

    DFA_transfer_table = dict()

    for i in count():
        if i >= len(DFA_list):
            break

        for char in all_chars:
            current_state = DFA_list[i]
            new_state = next_state(current_state, char, followpos_table, pos_table)
            if new_state in DFA_list:
                state_index = DFA_list.index(new_state)
            else: # new state not in list
                state_index = len(DFA_list)
                DFA_list.append(new_state)

            DFA_transfer_table[(i, char)] = state_index

    return (DFA_list, DFA_transfer_table)

def print_result(followpos_table, DFA_list, DFA_transfer_table, maxpos):
    print('\nfollowpos\n')
    for (x, y) in followpos_table.items():
        print('{} | {}'.format(x, y))

    print('\nstate <-> position set\n')
    for (i, state) in enumerate(DFA_list):
        print('{} = {}'.format(chr(i+65), state))

    g = Digraph()
    print('\nstate transfer table\n')
    for ((from_, char), to) in DFA_transfer_table.items():
        from_chr = chr(from_+65)
        to_chr = chr(to+65)
        is_terminal = maxpos in DFA_list[to]
        print('{} --{}--> {}{}'.format(
            from_chr, char, to_chr, ' #' if is_terminal else ''
        ))
        shape = 'doublecircle' if is_terminal else 'circle'
        g.node(to_chr, shape = shape)
        g.edge(from_chr, to_chr, label = char)

    g.render('result.gv', format='png')

def main():
    # !! hardcode the tree !!
    root = RATNode('.')
    root.right = RATNode('#')
    root.left = RATNode('.')
    root.left.right = RATNode('*')
    root.left.right.left = RATNode('|')
    root.left.right.left.left = RATNode('a')
    root.left.right.left.right = RATNode('b')
    root.left.left = RATNode('.')
    root.left.left.right = RATNode('b')
    root.left.left.left = RATNode('.')
    root.left.left.left.right = RATNode('b')
    root.left.left.left.left = RATNode('.')
    root.left.left.left.left.right = RATNode('a')
    root.left.left.left.left.left = RATNode('*')
    root.left.left.left.left.left.left = RATNode('|')
    root.left.left.left.left.left.left.right = RATNode('b')
    root.left.left.left.left.left.left.left = RATNode('a')

    # Indexing all leaves
    (pos_table, dft_feed) = indexing_leaves()
    root.dft(dft_feed)

    # ...
    root.dft(eval_nullable_firstpos_lastpos)
    # def print_node(node):
    #     print(node.value)
    #     print(node.nullable)
    #     print(node.firstpos)
    #     print()
    # root.dft(print_node)

    # followpos
    maxpos = max(pos_table.keys())
    (followpos_table, dft_feed) = eval_followpos(maxpos)
    root.dft(dft_feed)

    # build DFA
    (DFA_list, DFA_transfer_table) = build_DFA(root.firstpos, followpos_table, pos_table, maxpos)
    print_result(followpos_table, DFA_list, DFA_transfer_table, maxpos)

if __name__ == '__main__':
    main()