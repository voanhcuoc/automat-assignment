> `(a|b)*abb(a|b)*`

# Cơ sở lý thuyết

_graphviz source: https://graphs.grevian.org/graph/6039934328635392_
![](automat_flowchart.png)

Ghi chú: dft = Depth-first traversal

Về `$`: Để tìm trạng thái bắt đầu của DFA, ta có cách khác không cần tính hết tất cả firstpos:
thêm tiền tố `$` vào biểu thức chính quy gia tố với vị trí là 0. Từ đó trạng thái bắt đầu `A = followpos(0)`.

Từ đó ta có thể thực hiện tính followpos đồng thời với firstpos.

Chứng minh: Ta nhận thấy rằng biểu thức sau gia tố `$`, trạng thái bắt đầu (initial state) kì thực
là `V = {0}`, nhưng đây rõ ràng là trạng thái không quan trọng vì từ `V` đến `A` có sự truyền `$`
mà bản chất là sự truyền rỗng đối với biểu thức trước gia tố. Vậy đối với biểu thức ban đầu,
`A` chính là _initial state_, `V` có thể gọi nôm na là _void state_.

# NFA

`a|b`

_graphviz source: https://graphs.grevian.org/graph/5630302292541440_

![](aorb.png)

`(a|b)*`

_graphviz source: https://graphs.grevian.org/graph/6199356602449920_

![](aorbstar.png)

`(a|b)*abb`

_graphviz source: https://graphs.grevian.org/graph/5142634928537600_

![](aorbstarabb.png)

`(a|b)*abb(a|b)*`

_graphviz source: https://graphs.grevian.org/graph/5631197927440384_

![](full.png)

# DFA

Kết quả thu được từ chương trình (source phía dưới kết quả):

![](result.gv.png)

```
followpos

1 | {1, 2, 3}
2 | {1, 2, 3}
3 | {4}
4 | {5}
5 | {8, 6, 7}
6 | {8, 6, 7}
7 | {8, 6, 7}

state <-> position set

A = {1, 2, 3}
B = {1, 2, 3, 4}
C = {1, 2, 3, 5}
D = {1, 2, 3, 6, 7, 8}
E = {1, 2, 3, 4, 6, 7, 8}
F = {1, 2, 3, 5, 6, 7, 8}

state transfer table

A --b--> A
A --a--> B
B --b--> C
B --a--> B
C --b--> D #
C --a--> B
D --b--> D #
D --a--> E #
E --b--> F #
E --a--> E #
F --b--> D #
F --a--> E #

```

Source:

```python
from typing import List, Tuple, Set, Dict
from itertools import count, repeat
from graphviz import Digraph, render

class RATNode: # Regular expression Analysis Tree
    value: str
    left: 'RATNode'
    right: 'RATNode'
    nullable: bool
    firstpos: List[int]
    lastpos: List[int]

    def __init__(self, value):
        self.value = value
        self.left = None
        self.right = None

    def is_leaf(self):
        return (not self.left) and (not self.right)

    def dft(self, func):
        'Depth-first traversal'
        if self.left:
            self.left.dft(func)
        if self.right:
            self.right.dft(func)
        func(self)

def indexing_leaves():
    counter = count(1)
    pos_table = dict()

    def dft_feed(node):
        if node.is_leaf():
            i = next(counter)
            node.firstpos = node.lastpos = {i}
            node.nullable = False
            pos_table[i] = node.value

    return (pos_table, dft_feed)

def eval_nullable_firstpos_lastpos(node):
    if node.value == '|':
        node.nullable = node.left.nullable or node.right.nullable
        node.firstpos = node.left.firstpos.union(node.right.firstpos)
        node.lastpos = node.left.lastpos.union(node.right.lastpos)
    elif node.value == '*':
        node.nullable = True
        node.firstpos = node.left.firstpos
        node.lastpos = node.left.lastpos
    elif node.value == '.':
        node.nullable = node.left.nullable and node.right.nullable
        node.firstpos = (node.left.firstpos.union(node.right.firstpos) 
            if node.left.nullable else node.left.firstpos
        )
        node.lastpos = (node.right.lastpos.union(node.left.lastpos) 
            if node.right.nullable else node.right.lastpos
        )

def eval_followpos(maxpos):
    result = dict()
    for i in range(1, maxpos):
        result[i] = set()

    def dft_feed(node):
        if node.value == '*':
            for lpos in node.lastpos:
                for fpos in node.firstpos:
                    result[lpos].add(fpos)
        elif node.value == '.':
            for lpos in node.left.lastpos:
                for fpos in node.right.firstpos:
                    result[lpos].add(fpos)

    return (result, dft_feed)

def next_state(state, char, followpos_table, pos_table):
    result = set()
    for pos in state:
        if pos_table[pos] == char:
            result = result.union(followpos_table[pos])

    return result

def build_DFA(initial_state, followpos_table, pos_table, maxpos):

    all_chars = set(pos_table.values())
    all_chars.remove('#')

    DFA_list = list()
    DFA_list.append(initial_state)

    DFA_transfer_table = dict()

    for i in count():
        if i >= len(DFA_list):
            break

        for char in all_chars:
            current_state = DFA_list[i]
            new_state = next_state(current_state, char, followpos_table, pos_table)
            if new_state in DFA_list:
                state_index = DFA_list.index(new_state)
            else: # new state not in list
                state_index = len(DFA_list)
                DFA_list.append(new_state)

            DFA_transfer_table[(i, char)] = state_index

    return (DFA_list, DFA_transfer_table)

def print_result(followpos_table, DFA_list, DFA_transfer_table, maxpos):
    print('\nfollowpos\n')
    for (x, y) in followpos_table.items():
        print('{} | {}'.format(x, y))

    print('\nstate <-> position set\n')
    for (i, state) in enumerate(DFA_list):
        print('{} = {}'.format(chr(i+65), state))

    g = Digraph()
    print('\nstate transfer table\n')
    for ((from_, char), to) in DFA_transfer_table.items():
        from_chr = chr(from_+65)
        to_chr = chr(to+65)
        is_terminal = maxpos in DFA_list[to]
        print('{} --{}--> {}{}'.format(
            from_chr, char, to_chr, ' #' if is_terminal else ''
        ))
        shape = 'doublecircle' if is_terminal else 'circle'
        g.node(to_chr, shape = shape)
        g.edge(from_chr, to_chr, label = char)

    g.render('result.gv', format='png')

def main():
    # !! hardcode the tree !!
    root = RATNode('.')
    root.right = RATNode('#')
    root.left = RATNode('.')
    root.left.right = RATNode('*')
    root.left.right.left = RATNode('|')
    root.left.right.left.left = RATNode('a')
    root.left.right.left.right = RATNode('b')
    root.left.left = RATNode('.')
    root.left.left.right = RATNode('b')
    root.left.left.left = RATNode('.')
    root.left.left.left.right = RATNode('b')
    root.left.left.left.left = RATNode('.')
    root.left.left.left.left.right = RATNode('a')
    root.left.left.left.left.left = RATNode('*')
    root.left.left.left.left.left.left = RATNode('|')
    root.left.left.left.left.left.left.right = RATNode('b')
    root.left.left.left.left.left.left.left = RATNode('a')

    # Indexing all leaves
    (pos_table, dft_feed) = indexing_leaves()
    root.dft(dft_feed)

    # ...
    root.dft(eval_nullable_firstpos_lastpos)
    # def print_node(node):
    #     print(node.value)
    #     print(node.nullable)
    #     print(node.firstpos)
    #     print()
    # root.dft(print_node)

    # followpos
    maxpos = max(pos_table.keys())
    (followpos_table, dft_feed) = eval_followpos(maxpos)
    root.dft(dft_feed)

    # build DFA
    (DFA_list, DFA_transfer_table) = build_DFA(root.firstpos, followpos_table, pos_table, maxpos)
    print_result(followpos_table, DFA_list, DFA_transfer_table, maxpos)

if __name__ == '__main__':
    main()
```
